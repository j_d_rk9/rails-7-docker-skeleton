# Depicrated since 10th of October 2024

This framework is no longer maintened as Ruby on Rails now comes with Docker config, this repo will remain avaible for historic purposes. Please don't use this anymore. 

# Docker Compose: Ruby on Rails Example

A simple example of using Docker Compose for developing and deploying
Ruby on Rails applications. This is a skeleton and framework to just develop. 

## 

  **Versions**
  ```
  Ruby version: ruby 3.1.0p0 (2021-12-25 revision fb4df44d16) [x86_64-linux]
  Rails version: 7.0.1
  NodeJS v12.22.5
  bootstrap 5.1.3
  jquery 3.6.0
  popper.js 1.16.1

  ```
The idea is to have latest possible versions, the container has debian base image providing flexibility.
On the road map is still some automation deployment with VAGRANT, ANSIBLE or PUPPET.

## Getting started

follow these steps:

  * Destroy any existing containers with conflicting names.
  * Copy `.env.example` to `.env` and modify (if required).
  * Create `config/database.yml` based on `.docker/ror/example.database.yml`.
  * Build an image for Ruby on Rails:
    ```
    docker-compose build
    ```
  * Boot up the app by running:
    ```
    docker-compose up
    ```
  * Create databases by running:
    ```
      docker-compose exec web rake db:create
    ```

Cool! Now your Rails app should be available at
[localhost:3000](http://localhost:3000).

### New app

If you want to start with a Rails app from scratch,

  * Create the following files:
    * `Gemfile`: You can base this on `.docker/ror/example.Gemfile`.
    * `Gemfile.lock`: An empty file.
  * Run the following command to install Rails. You can modify the params for
    the `rails new` command as needed.
    ```
    docker-compose run --no-deps web rails new . --force --database=postgresql
    ```
    * For a quick Rails install, use the `--minimal` flag.

### rails specefic command 

New ruby app
    ```
    docker-compose run web rails new . --force --database=postgresql
    ```
Creating a Migration
    ```
    docker-compose run web rails generate migration
    ```
Create a controller with Articles#index
    ```
    docker-compose run web rails generate controller Articles index 
    ```
Create a model 
    ```
    docker-compose run web rails generate model Article title:string body:text
    ```
db migration
    ```
    docker-compose run web rails db:migrate 
    ```  
Refer to the rails documentation for more: https://guides.rubyonrails.org

** web is the container running the rails server **

## Commands

Here are some helpful commands.

  * `docker-compose start`: Starts app containers.
  * `docker-compose stop`: Stops app containers.
  * `rake -T`: For some other helpful commands.

See [docker compose docs](https://docs.docker.com/compose/) for further info on
Docker Compose.

[!["Buy Me A Coffee"](https://www.buymeacoffee.com/assets/img/custom_images/orange_img.png)](https://www.buymeacoffee.com/jdrk9)
